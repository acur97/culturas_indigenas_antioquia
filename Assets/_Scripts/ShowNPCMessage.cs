﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowNPCMessage : MonoBehaviour
{
    public GameObject UIMessage;
    public string messageToShow;
    public void showNPCMessage()
    {
        UIMessage.SetActive(true);
        Text UIMessageText = UIMessage.GetComponentInChildren<Text>();
        UIMessageText.text = messageToShow;
    }
}
