﻿Shader "Standard Tessellate Phong"
{
    Properties
    {
        //Tessellation
        _EdgeLength("Edge length", Range(2,50)) = 12.5
        _Phong("Phong Strengh", Range(-1,1)) = 0.5

        //Texture properties
        _MainTex("Base (RGB)", 2D) = "white" {}
        _Color("Color", color) = (1,1,1,0)
        _NormalMap("Normalmap", 2D) = "bump" {}
        _NormalPow("Normal Power", Range(-1, 1)) = 1
        _ParallaxMap("HeightMap", 2D) = "white" {}
        _Parallax("Height", Range(0, 1)) = 0
        _OccMap("Occlusion Map",2D) = "white"{}
        _OcclusionStrength("Occlusion Strenght", Range(0, 1)) = 0
        _EmissionMap("Emission Map",2D) = "black"{}
        [HDR]_EmissionColor("EmissionColor", color) = (0,0,0,0)
        _Metallic("Metallic", Range(0,1)) = 0.0
        _Glossiness("Smoothness", Range(0,1)) = 0.2

    }
        SubShader
        {
            Tags { "RenderType" = "Opaque" }
            LOD 300

            CGPROGRAM
            #pragma surface surf Standard addshadow fullforwardshadows vertex:disp tessellate:tessEdge tessphong:_Phong
            #pragma require tessellation tessHW
            #pragma target 3.0

            #include "UnityPBSLighting.cginc"
            #include "Tessellation.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };

            float _Phong;
            float _EdgeLength;

            float4 tessEdge(appdata v0, appdata v1, appdata v2)
            {
                return UnityEdgeLengthBasedTess(v0.vertex, v1.vertex, v2.vertex, _EdgeLength);
            }

            void disp(inout appdata v)
            {
                //Do nothing
            }

            struct Input {
                float2 uv_MainTex;
                float2 uv_NormalMap;
                float3 viewDir;
            };


            half _Glossiness;
            half _Metallic;

            sampler2D _MainTex;
            sampler2D _NormalMap;
            sampler2D _OccMap;
            sampler2D _EmissionMap;
            sampler2D _ParallaxMap;

            fixed4 _Color;
            fixed4 _EmissionColor;

            float _NormalPow;
            float _Parallax;
            float _OcclusionStrength;

            void surf(Input IN, inout SurfaceOutputStandard o)
            {

                half h = tex2D(_ParallaxMap, IN.uv_MainTex).g;
                float2 offset = ParallaxOffset1Step(h, _Parallax, IN.viewDir);
                IN.uv_MainTex += offset;
                IN.uv_NormalMap += offset;

                half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
                half4 O = tex2D(_OccMap, IN.uv_MainTex);

                o.Albedo = c.rgb;
                o.Metallic = _Metallic;
                o.Smoothness = _Glossiness;
                o.Occlusion = LerpOneTo(O.g, _OcclusionStrength);
                o.Emission = tex2D(_EmissionMap, IN.uv_MainTex).rgb * _EmissionColor.rgb;
                o.Normal = UnpackScaleNormal(tex2D(_NormalMap, IN.uv_NormalMap), _NormalPow);

            }
            ENDCG
        }
            FallBack "Diffuse"
}