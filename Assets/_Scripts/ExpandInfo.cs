﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpandInfo : MonoBehaviour
{
    public TextMeshPro textMesh;
    public int cont = 0, limitMax = 5;
    public string[] texts;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (cont > 0)
            {
                cont--;
                textMesh.text = texts[cont];
            }
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (cont < limitMax)
            {
                cont++;
                textMesh.text = texts[cont];
            }

        }
    }
    public void ResetValues()
    {
        cont = 0;
        textMesh.text = texts[cont];
    }
}
