﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLookCam : MonoBehaviour
{
    
    public Transform cam;
    public float minDistance = 25;
    public float maxDistance = 50;
    public float minScale = 0.1f;
    public float maxScale = 1;

    private float distance = 0;
    private float scale = 0;

    private void Awake()
    {
        scale = minScale;
        transform.localScale = new Vector3(scale, scale, scale);
    }

    private void Update()
    {
        transform.LookAt(cam);

        distance = Vector3.Distance(cam.position, transform.position);

        if (distance <= maxDistance && distance >= minDistance)
        {
            scale = maxDistance - distance;
            scale /= (maxDistance - minDistance);
            scale = Mathf.Clamp(scale, minScale, maxScale);
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}