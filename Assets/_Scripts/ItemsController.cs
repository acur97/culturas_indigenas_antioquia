﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemsController : MonoBehaviour
{
    public Slider barraSlider;
    public int items = 0;
    public GameObject currentNPC;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Item")
        {
            Destroy(other.gameObject);
            items++;
            barraSlider.value = items;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="NPC")
        {
            collision.gameObject.GetComponent<ShowNPCMessage>().showNPCMessage();
            currentNPC = collision.gameObject;
        }
    }
}
