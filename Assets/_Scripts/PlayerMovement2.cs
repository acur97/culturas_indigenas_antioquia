﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement2 : MonoBehaviour
{
    Rigidbody playerRB;
    public float speed = 100;
    public float jumpForce = 5;
    Animator animator;
    public bool canJump = true;
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.W))
        {
            playerRB.velocity = new Vector3(playerRB.velocity.x, playerRB.velocity.y, speed * Time.deltaTime);
        }
        animator.SetFloat("speed", playerRB.velocity.z);

        if(Input.GetButtonDown("Jump") &&canJump)
        {
            canJump = false;
            playerRB.AddForce(Vector3.up * jumpForce * Time.deltaTime,ForceMode.Impulse); 
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="Floor")
        {
            canJump = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            canJump = false;
        }
    }
}
