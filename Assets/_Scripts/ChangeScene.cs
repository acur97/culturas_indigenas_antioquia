﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public GameObject MessageUI;

    private void OnTriggerEnter(Collider other)
    {
        MessageUI.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        MessageUI.SetActive(false);
    }

    public void GoToGame()
    {
        SceneManager.LoadScene(1);
    }
}
