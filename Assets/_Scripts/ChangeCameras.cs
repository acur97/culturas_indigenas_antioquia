﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Cinemachine.Examples;
using UnityTemplateProjects;

public class ChangeCameras : MonoBehaviour
{
    public bool puedeCambiar = true;
    public CinemachineVirtualCameraBase CM3dPerson;
    public CinemachineVirtualCameraBase CMFlyCam;
    [Space]
    public Animator MujerAnim;
    [Space]
    public Animator animatorJugable;
    public Animator animatorIdle;
    [Space]
    public CharacterMovement cMovement;
    public SimpleCameraController sCcontroller;
    [Space]
    public KeyCode teclaCambio = KeyCode.Space;
    private bool cam3dPersona = true;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;

        CM3dPerson.Priority = 10;
        CMFlyCam.Priority = 9;

        MujerAnim.runtimeAnimatorController = animatorJugable.runtimeAnimatorController;

        sCcontroller.enabled = false;
        cMovement.enabled = true;
    }

    public void PuedesCambiar(bool puede)
    {
        puedeCambiar = puede;
    }

    private void Update()
    {
#if !UNITY_WEBGL
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
#endif
        if (puedeCambiar && Input.GetKeyDown(teclaCambio))
        {
            if (cam3dPersona)
            {
                Cursor.lockState = CursorLockMode.None;

                CMFlyCam.transform.position = CM3dPerson.transform.position;
                CMFlyCam.transform.rotation = Quaternion.identity;
                //CMFlyCam.transform.rotation = CM3dPerson.transform.rotation;

                CM3dPerson.Priority = 10;
                CMFlyCam.Priority = 11;

                MujerAnim.runtimeAnimatorController = animatorIdle.runtimeAnimatorController;

                sCcontroller.enabled = true;
                cMovement.enabled = false;

                cam3dPersona = false;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                //Cursor.lockState = CursorLockMode.None;

                CM3dPerson.Priority = 10;
                CMFlyCam.Priority = 9;

                MujerAnim.runtimeAnimatorController = animatorJugable.runtimeAnimatorController;

                sCcontroller.enabled = false;
                cMovement.enabled = true;

                cam3dPersona = true;
            }
        }
    }
}