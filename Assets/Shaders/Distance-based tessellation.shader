﻿Shader "Distance-based tessellation" {
    Properties{
        _Tess("Tessellation", Range(1,32)) = 4
        _MinTess("Min distance", Range(0,100)) = 10
        _MaxTess("Max distance", Range(0,100)) = 25
        _MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
        _Cutoff("Alpha cutoff", Range(0,1)) = 0.5
        _DispTex("Disp Texture", 2D) = "gray" {}
        _NormalMap("Normalmap", 2D) = "bump" {}
        _NormalPow("Normal Power", Range(-10,10)) = 1.0
        _NormalMap2("Normalmap2", 2D) = "bump" {}
        _NormalPow2("Normal Power2", Range(-10,10)) = 1.0
        _NormalMap3("Normalmap3", 2D) = "bump" {}
        _NormalPow3("Normal Power3", Range(-10,10)) = 1.0
        _Displacement("Displacement", Range(0, 1.0)) = 0.3
        _Color("Color", color) = (1,1,1,0)
        //_SpecColor("Spec color", color) = (0.5,0.5,0.5,0.5)
        _Metallic("Metallic", Range(-1,1)) = 0.0
        _Glossiness("Smoothness", Range(-1,1)) = 0.2
    }
        SubShader{
            //Tags { "RenderType" = "Opaque" }
            //Tags {"Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout"}
            Tags {"Queue" = "AlphaTest" "RenderType" = "TransparentCutout"}
            Cull Off
            LOD 300

            CGPROGRAM
            #pragma surface surf Standard alphatest:_Cutoff addshadow fullforwardshadows vertex:disp tessellate:tessDistance
            #pragma require tessellation tessHW
            #pragma target 4.6
            #include "UnityPBSLighting.cginc"
            #include "Tessellation.cginc"

            struct appdata {
                float4 vertex : POSITION;
                float4 tangent : TANGENT;
                float3 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

            float _Tess;
            float _MinTess;
            float _MaxTess;

            float4 tessDistance(appdata v0, appdata v1, appdata v2) {
                //float minDist = 10;
                //float maxDist = 25;
                return UnityDistanceBasedTess(v0.vertex, v1.vertex, v2.vertex, _MinTess, _MaxTess, _Tess);
            }

            sampler2D _DispTex;
            float _Displacement;

            void disp(inout appdata v)
            {
                float d = tex2Dlod(_DispTex, float4(v.texcoord.xy,0,0)).r * _Displacement;
                v.vertex.xyz += v.normal * d;
            }

            struct Input {
                float2 uv_MainTex;
                float2 uv_NormalMap;
                float2 uv_NormalMap2;
                float2 uv_NormalMap3;
            };

            half _Glossiness;
            half _Metallic;
            half _NormalPow;
            half _NormalPow2;
            half _NormalPow3;

            sampler2D _MainTex;
            sampler2D _NormalMap;
            sampler2D _NormalMap2;
            sampler2D _NormalMap3;
            fixed4 _Color;

            fixed3 baseNormal;
            fixed3 detailNormal;
            fixed3 miniNormal;
            fixed3 combinedNormal;
            fixed3 combinedNormal2;

            inline fixed3 combineNormalMaps(fixed3 base, fixed3 detail) {
                base += fixed3(0, 0, 1);
                detail *= fixed3(-1, -1, 1);
                return base * dot(base, detail) / base.z - detail;
            }

            void surf(Input IN, inout SurfaceOutputStandard o) {
                half4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
                o.Albedo = c.rgb;
                o.Alpha = c.a;
                //o.Specular = 0.2;
                //o.Gloss = 1.0;
                o.Metallic = _Metallic;
                o.Smoothness = _Glossiness;
                //float4 nor = tex2D(_NormalMap, IN.uv_NormalMap);
                //o.Normal = UnpackNormal(nor);
                //o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap) + tex2D(_NormalMap2, IN.uv_NormalMap2) * 2 - 1);
                
                baseNormal = UnpackScaleNormal(tex2D(_NormalMap, IN.uv_NormalMap), _NormalPow);
                detailNormal = UnpackScaleNormal(tex2D(_NormalMap2, IN.uv_NormalMap2), _NormalPow2);
                miniNormal = UnpackScaleNormal(tex2D(_NormalMap3, IN.uv_NormalMap3), _NormalPow3);
                combinedNormal = combineNormalMaps(baseNormal, detailNormal);
                combinedNormal2 = combineNormalMaps(combinedNormal, miniNormal);

                o.Normal = combinedNormal2;
            }
            ENDCG
        }
            //FallBack "Diffuse"
            FallBack "Transparent/Cutout/Diffuse"
}