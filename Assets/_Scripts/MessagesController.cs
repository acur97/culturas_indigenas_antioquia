﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessagesController : MonoBehaviour
{
    public GameObject MessageScreen;
    Text messageText;
    Button nextButton;
    public string[] messagesArray;
    public int messagesController = 0;
    public bool InGameplay = false;
    public PlayerMovement playerMovement;
    public GameObject fadeInPanel;
    public Camera mainCamera, playerCamera;
    public ItemsController itemsController;
    void Start()
    {
        messageText = MessageScreen.GetComponentInChildren<Text>();
        messageText.text = messagesArray[0];
        MessageScreen.SetActive(true);
        nextButton = MessageScreen.GetComponentInChildren<Button>();

        if(!InGameplay)
        {
            playerMovement.enabled = false;
        }
    }
    public void EnableGamePlay()
    {
        InGameplay = true;
        playerMovement.enabled = true;
        playerMovement.gameObject.transform.Rotate(new Vector3(0, -180, 0));
        fadeInPanel.SetActive(true);
        MessageScreen.SetActive(false);
        playerCamera.enabled = true;
        mainCamera.enabled = false;
    }

    public void NextMessage()
    {
        if(messagesController==4)
        {
            Text ButtonText = nextButton.GetComponentInChildren<Text>();
            ButtonText.text = "Continuar";
        }
        if(messagesController==5)
        {
            EnableGamePlay();
        }
        messagesController++;
        if(messagesController<=5)
        {
            messageText.text = messagesArray[messagesController];
        }
       

        if(messagesController>=6)
        {
            fadeInPanel.SetActive(false);
            fadeInPanel.SetActive(true);

            MessageScreen.SetActive(false);
            Destroy(itemsController.currentNPC);
        }
    }
}
