﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;

public class TriggerTexts : MonoBehaviour
{
    [TextArea]
    public string[] textBox;
    [Space]
    public float velocidad = 2;
    public TextMeshProUGUI textU;
    private int contador = 0;
    public Button btnNext;
    public Image img1;
    public Image img2;
    [Space]
    public UnityEvent alAcabar;

    private float speed1;
    private bool primero = true;
    private float speed2;

    private bool updateUI = false;

    public void SiguienteText()
    {
        if (primero)
        {
            textU.text = textBox[0];
            StartCoroutine(ChangeSpeed1(0, 191, velocidad));
            StartCoroutine(ChangeSpeed2(0, 255, velocidad));
            primero = false;
        }
        else if (contador < textBox.Length)
        {
            textU.text = textBox[contador];
            StartCoroutine(ChangeSpeed2(0, 255, velocidad));
        }
        else
        {
            alAcabar.Invoke();
        }
        contador++;
    }

    public void Update()
    {
        if (primero && updateUI)
        {
            img1.color = new Color32(0, 0, 0, (byte)speed1);
            img2.color = new Color32(0, 0, 0, (byte)speed1);
            textU.color = new Color32(255, 255, 255, (byte)speed2);
        }
        if (!primero && updateUI)
        {
            textU.color = new Color32(255, 255, 255, (byte)speed2);
        }
    }

    IEnumerator ChangeSpeed1(float v_start, float v_end, float duration)
    {
        updateUI = true;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            speed1 = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        speed1 = v_end;
        updateUI = false;
    }
    IEnumerator ChangeSpeed2(float v_start, float v_end, float duration)
    {
        updateUI = true;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            speed2 = Mathf.Lerp(v_start, v_end, elapsed / duration);
            elapsed += Time.deltaTime;
            yield return null;
        }
        speed2 = v_end;
        updateUI = false;
    }
}