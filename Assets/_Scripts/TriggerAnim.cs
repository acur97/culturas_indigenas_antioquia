﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine.Examples;
using UnityTemplateProjects;

public class TriggerAnim : MonoBehaviour
{
    public Animator anim;
    private Animator animStarts;
    [Space]
    public Animator animatorStarts;
    [Space]
    public CharacterMovement cMovement;
    public SimpleCameraController sCcontroller;
    public ChangeCameras cCameras;

    private int contador = 1;

    private void Awake()
    {
        animStarts = GetComponent<Animator>();
        cCameras.enabled = false;
        sCcontroller.enabled = false;
        cMovement.enabled = false;

        anim.runtimeAnimatorController = animatorStarts.runtimeAnimatorController;
    }

    public void Trig(string _trigger)
    {
        anim.SetTrigger(_trigger);
    }

    public void Siguiente()
    {
        contador++;
        if (contador == 5)
        {
            FinalAnims();
        }
        else
        {
            animStarts.SetTrigger(contador.ToString());
        }
    }

    public void FinalAnims()
    {
        cCameras.enabled = true;
        gameObject.SetActive(false);
    }
}